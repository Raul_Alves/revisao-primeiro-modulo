package modulo01.aula01_variaveis;

public class VariaveisPontoFlutuante {

	public static void main(String[] args) {
		
		double valorCincoCasasDecimais = 2.90000;
		double valorQuatroCasasDecimais = 2.9001;

		float valorTresCasasDecimais = 3.955F;
		float valorSeteCasasDecimais = 3.9500000F;
		float valorSeteCasasDecimais2 = 3.950F;

		System.out.println("Valor Cinco Casas Decimais = " + valorCincoCasasDecimais);
		System.out.println("Valor Quatro Casas Decimais = " + valorQuatroCasasDecimais);
		System.out.println("Valor Tres Casas Decimais = " + valorTresCasasDecimais);
		System.out.println("Valor Sete Casas Decimais = " + valorSeteCasasDecimais);
		System.out.println("Valor Sete Casas Decimais = " + String.format("%.7f", valorSeteCasasDecimais2));
	}

}
