package modulo01.aula03_operadores;

public class CurtoCircuito {

	public static void main(String[] args) {
		
		boolean verdadeiro = true;
		boolean falso = false;

		boolean resultado1 = falso & verdadeiro;
		boolean resultado2 = falso && verdadeiro;

		System.out.println("Resultado 1: " + resultado1);
		System.out.println("Resultado 2: " + resultado2);

		boolean resultado3 = falso || verdadeiro;
		boolean resultado4 = verdadeiro || verdadeiro;
		boolean resultado5 = falso || falso;

		System.out.println("Resultado 3: " + resultado3);
		System.out.println("Resultado 4: " + resultado4);
		System.out.println("Resultado 5: " + resultado5);

		
		int resultado = 1 + 1 - 1 + 1 * 1 / 1;
		int resultado6 = 1 + 1 - (1 + 1) * 1 / 1;

		System.out.println(resultado);
		System.out.println(resultado6);
	}

}
